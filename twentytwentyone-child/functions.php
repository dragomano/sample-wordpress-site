<?php

add_action('wp_footer', 'add_custom_block');
function add_custom_block() {
	if (!is_home()) return;

	?>
	<div class="custom_top_block"></div>
	<?php
}

add_action('wp_enqueue_scripts', 'load_theme_styles');
function load_theme_styles() {
	wp_enqueue_style('twenty-twenty-one-child-style', get_stylesheet_directory_uri() . '/style.css', array('twenty-twenty-one-style'));
}
